from django.shortcuts import render
from django.views.generic.base import TemplateView

from rest_framework.viewsets import ModelViewSet

from Category.models import Category
from Item.models import Item, ItemImage, ExtraInfo
from Item.serializers import ItemReadSerializer


class IndexView(TemplateView):
    template_name = "index.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['items'] = Item.objects.all()
        context['categories'] = Category.objects.all()
        return context


class CategoryDetailView(TemplateView):
    template_name = "item/CategoryDetail.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        category = Category.objects.get(pk=self.kwargs.get('pk'))
        context['items'] = Item.objects.filter(category_id=category.id)
        context['categories'] = Category.objects.exclude(pk=category.id)
        context['activeCategory'] = category
        return context

class ItemDetailView(TemplateView):
    template_name = "item/ItemDetail.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        item = Item.objects.filter(pk=self.kwargs.get('pk')).first()
        context['item'] = item
        context['items'] = Item.objects.filter(category_id=item.category_id).exclude(pk=item.id)[:6]
        context['extra'] = ExtraInfo.objects.filter(item_id=self.kwargs.get('pk'))
        context['images'] = ItemImage.objects.filter(item_id=self.kwargs.get('pk'))
        context['categories'] = Category.objects.exclude(pk=item.category_id)
        context['activeCategory'] = Category.objects.get(pk=item.category_id)
        print(context['extra'])
        return context
