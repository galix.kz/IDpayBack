from rest_framework.serializers import ModelSerializer

from Category.serializers import CategorySerializer
from Item.models import Item


class ItemReadSerializer(ModelSerializer):
    category = CategorySerializer

    class Meta:
        model = Item
        fields = '__all__'
