from django.db import models
from django.utils.safestring import mark_safe

from Category.models import Category


class Item(models.Model):
    category = models.ForeignKey(Category, on_delete=models.CASCADE, related_name='items')
    name = models.CharField(max_length=300)
    photo = models.ImageField(null=True, default=None, blank=True)
    price = models.IntegerField(null=True)
    description = models.TextField(null=True)

    def __str__(self):
        return self.name

    def image_tag(self):
        return mark_safe('<img src="/media/%s" width="150" height="150" />' % self.photo)

    image_tag.short_description = 'Image'

    class Meta:
        verbose_name = "Товары"
        verbose_name_plural = "Товары"


class ItemImage(models.Model):
    item = models.ForeignKey('Item.Item', on_delete=models.CASCADE, related_name="item")
    image = models.ImageField()

    def image_tag(self):
        return mark_safe('<img src="/media/%s" width="150" height="150" />' % self.image)

    image_tag.short_description = 'Image'

    class Meta:
        verbose_name = "Фото к товарам"
        verbose_name_plural = "Фото к товарам"

class ExtraInfo(models.Model):
    item = models.ForeignKey('Item.Item', on_delete=models.CASCADE, related_name="info")
    title = models.CharField(max_length=250)
    description = models.TextField()