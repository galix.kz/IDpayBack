from django.contrib import admin
from django.utils.safestring import mark_safe

from .models import Item, ItemImage, ExtraInfo


class ImageAdmin(admin.ModelAdmin):
    search_fields = ('item__name',)
    list_display = ('id', 'item', 'image_tag')

class ExtraInline(admin.TabularInline):
    model = ExtraInfo
    fields = ('title', 'description')
    can_delete = True

class ImageInline(admin.TabularInline):
    model = ItemImage
    fields = ('image','image_tag' )
    readonly_fields = ('image_tag',)
    can_delete = True
    def image_tag(self, obj):
        return mark_safe('<img src="/media/%s" width="150" height="150" />' % obj.image)


class ItemAdmin(admin.ModelAdmin):
    inlines = [
        ImageInline,
        ExtraInline
    ]


admin.site.register(Item, ItemAdmin)
admin.site.register(ItemImage, ImageAdmin)
# Register your models here.
