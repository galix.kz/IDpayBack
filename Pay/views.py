import json

from django.http import HttpResponse, JsonResponse
from django.shortcuts import render
from django.utils.decorators import method_decorator
from django.views import View
from django.views.decorators.csrf import csrf_exempt
from django.views.generic import TemplateView

from Auth.models import User, UserNotInDatabase
from Item.models import Item
from Pay.models import PaymentHistory, BoughtItem


@method_decorator(csrf_exempt, name='dispatch')
class PaymentView(View):

    @csrf_exempt
    def post(self, request, *args, **kwargs):
        data = json.loads(request.body)
        user = User.objects.filter(UID=data['user'], type=User.UsersType.BRANCH)
        if len(user) != 1:
            print('no such user')
            return HttpResponse(status=400)
        user = user[0]
        count = 0
        items_total_price_list = {}
        for item in data['items']:
            i = Item.objects.get(pk=item['id'])
            item_total_price = i.price * item['count']
            count = count + item_total_price
            items_total_price_list[str(i.id)] = item_total_price
        paymentHistory = PaymentHistory.objects.create(cafe_id=user.id, total_price=count)
        for item in data['items']:
            print(items_total_price_list)
            BoughtItem.objects.create(
                total_price=items_total_price_list[item['id']], count=item['count'],
                item_id=item['id'], payment_history=paymentHistory)
        return HttpResponse(paymentHistory.id)

    def put(self, request, *args, **kwargs):
        data = json.loads(request.body)
        print(data)
        cafe = User.objects.filter(UID=data['cafe'], type=User.UsersType.BRANCH)
        if len(cafe) != 1:
            print('no such cafe')
            return HttpResponse(status=400)
        cafe = cafe[0]
        user = User.objects.filter(UID=data['user'], type=User.UsersType.STUDENT)
        if len(user) != 1:
            print('new user')
            not_in_db = UserNotInDatabase.objects.create(user=cafe, uid=data['user'])
            return HttpResponse(json.dumps(data['user']), content_type="application/json")
        user = user[0]

        payment = PaymentHistory.objects.set_paid_user(user, cafe)
        if not payment:
            return HttpResponse(json.dumps("Payment Error"), content_type="application/json")
        return HttpResponse(json.dumps("Balance %s" % user.balance), content_type="application/json")

    def patch(self, request, *args, **kwargs):
        data = json.loads(request.body)
        payment = PaymentHistory.objects.get(pk=data['id'])
        payment.status = PaymentHistory.Status.CANCELED
        payment.save()
        return HttpResponse('ok')

class PaymentDetails(TemplateView):
    template_name = "item/PaymentDetail.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        paymentHistory = PaymentHistory.objects.filter(pk=self.kwargs.get('pk')).first()
        context['history'] = paymentHistory
        return context
