from django.urls import reverse

from django.contrib import admin
from django.utils.html import format_html
from django.utils.safestring import mark_safe
from django.utils.html import escape
from .models import PaymentHistory,BoughtItem

# Register your models here.

class PaymentHistoryAdmin(admin.ModelAdmin):
    list_display = ["cafe","user", "total_price", 'payment_date']

admin.site.register(PaymentHistory, PaymentHistoryAdmin)


class BoughtItemsAdmin(admin.ModelAdmin):
    list_display = ["item", "count", 'link_to_user']

    def link_to_user(self, obj):
        link = reverse("admin:Pay_paymenthistory_change", args=[obj.payment_history.id])
        return mark_safe(f'<a href="{link}">{obj.payment_history}</a>')

    link_to_user.short_description = 'История покупок'
admin.site.register(BoughtItem, BoughtItemsAdmin)