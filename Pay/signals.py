from django.db.models.signals import post_save
from django.dispatch import receiver

from Pay.models import PaymentHistory, BoughtItem
from Auth.models import User

@receiver(post_save, sender=BoughtItem)
def count_total_price_of_bought_item(sender, instance, created, **kwargs):
    if created:
        instance.total_price = instance.count * instance.item.price
        instance.save()


@receiver(post_save, sender=PaymentHistory)
def change_payment_history(sender, instance, created, **kwargs):

    if created:
        instance.status = PaymentHistory.Status.WAITING_FOR_PAY
        instance.save()
    else:
        if instance.status == PaymentHistory.Status.CANCELED:
            if instance.user is not None:
                user = User.objects.get(pk=instance.user.id)
                user.balance = user.balance + instance.total_price
                user.save()
