from rest_framework.serializers import ModelSerializer, Serializer
from rest_framework import serializers
from Auth.serializers import UserSerializer
from Item.serializers import ItemReadSerializer
from Pay.models import PaymentHistory, BoughtItem


class SetPaidUserSerializer(Serializer):
    user_uid = serializers.CharField()
    cafe_uid = serializers.CharField()


class PaymentHistoryReadSerializer(ModelSerializer):
    paid_by = UserSerializer()

    class Meta:
        model = PaymentHistory
        fields = '__all__'


class PaymentHistoryWriteSerializer(ModelSerializer):
    class Meta:
        model = PaymentHistory
        fields = '__all__'


class BoughtWriteItemSerializer(ModelSerializer):
    class Meta:
        model = BoughtItem
        fields = '__all__'


class BoughtReadItemSerializer(ModelSerializer):
    item = ItemReadSerializer()
    payment_history = PaymentHistoryReadSerializer()

    class Meta:
        model = BoughtItem
        fields = '__all__'
