from django.apps import AppConfig


class PayConfig(AppConfig):
    name = 'Pay'

    def ready(self):
        # make payment usable
        import Pay.signals