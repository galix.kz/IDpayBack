from django.db import models

# Create your models here.
from Auth.models import User


class PaymentHistoryManager(models.Manager):
    def set_paid_user(self, user, cafe):
        if user is not None and cafe is not None:
            payment_history = super().get_queryset().filter(cafe_id=cafe.id). \
                filter(status=PaymentHistory.Status.WAITING_FOR_PAY).order_by('-payment_date').first()
            if payment_history is None:
                return True
            is_balance_changed = user.change_balance(payment_history.total_price)
            if not is_balance_changed:
                payment_history.status = PaymentHistory.Status.CANCELED
                payment_history.save()
                return False
            payment_history.user = user
            payment_history.status = PaymentHistory.Status.DONE
            print(payment_history.status)
            payment_history.save()
            return True


class PaymentHistory(models.Model):
    class Status:
        PREPARING = 0
        WAITING_FOR_PAY = 1
        DONE = 2
        CANCELED = 3

    cafe = models.ForeignKey('Auth.User', blank=True, null=True, on_delete=models.CASCADE,
                             related_name="cafe")
    payment_date = models.DateTimeField(auto_now_add=True)
    user = models.ForeignKey('Auth.User', on_delete=models.CASCADE, null=True, default=None, blank=True,
                             related_name="paid_by")
    total_price = models.IntegerField()
    status = models.IntegerField(default=Status.PREPARING)
    objects = PaymentHistoryManager()

    class Meta:
        verbose_name = "История покупок"
        verbose_name_plural = "История покупок"

    def __str__(self):
        if (self.user):
            return self.user.first_name + " " + self.user.last_name + " " + str(self.total_price)
        return str(self.total_price) + " Не оплачено"


class BoughtItem(models.Model):
    item = models.ForeignKey('Item.Item', blank=True, on_delete=models.CASCADE, verbose_name='Товар')
    count = models.IntegerField(verbose_name='Количество')
    total_price = models.IntegerField(null=True, blank=True, verbose_name='Сумма')
    payment_history = models.ForeignKey(PaymentHistory, on_delete=models.CASCADE, null=True, blank=True, verbose_name='История покупок')

    class Meta:
        verbose_name = "Купленные товары"
        verbose_name_plural = "Купленные товары"
