from rest_framework import serializers

from Category.models import Category
from Item.models import Item



class ItemSerializer(serializers.ModelSerializer):
    class Meta:
        model = Item
        fields = '__all__'


class CategorySerializer(serializers.ModelSerializer):
    items = ItemSerializer(many=True, read_only=True)

    class Meta:
        model = Category
        fields = ('name', 'shop', 'photo', 'items')
