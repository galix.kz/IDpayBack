from django.shortcuts import render
from rest_framework.viewsets import ModelViewSet

from Auth.models import User
from Category.models import Category
from Category.serializers import CategorySerializer
from Auth.permissions import IsAllowedToViewCategory


class CategoryViewSet(ModelViewSet):
    serializer_class = CategorySerializer
    queryset = Category.objects.all()
    permission_classes = (IsAllowedToViewCategory,)

    def get_queryset(self):
        user = self.request.user
        print(user)
        self.queryset = self.queryset.filter(shop=user)
        return self.queryset

