from django.contrib import admin
from .models import Category
from Auth.models import User
from django import forms


class CategoryForm(forms.ModelForm):
    class Meta:
        model = Category
        fields = ('shop','name', 'photo')

    def __init__(self, *args, **kwargs):
        super(CategoryForm, self).__init__(*args, **kwargs)
        if self.instance:  # Editing and existing instance
            self.fields['shop'].queryset = User.objects.filter(type=User.UsersType.BRANCH)

# Register your models here.
class CategoryAdmin(admin.ModelAdmin):
    search_fields = ("name","shop__username", "shop__first_name", "shop__last_name")
    list_display = ('shop', 'name')
    form = CategoryForm


admin.site.register(Category, CategoryAdmin)
