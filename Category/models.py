from django.db import models

from Auth.models import User


class Category(models.Model):
    shop = models.ForeignKey(User, on_delete=models.CASCADE)
    name = models.CharField(max_length=300)
    photo = models.ImageField(null=True, default=None, blank=True)

    def __str__(self):
        return self.shop.username + " " + self.name
    class Meta:
        verbose_name = "Категории"
        verbose_name_plural = "Категории"
