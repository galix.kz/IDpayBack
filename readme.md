# Tesla Pay

Payment system for Tesla education center

## Getting Started
Install Docker, docker-compose
```
docker-compose up
```
```
docker-compose exec web python manage.py migrate --noinput
```
```
docker-compose exec web python manage.py createsuperuser
```