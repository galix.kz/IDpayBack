from django.shortcuts import render

# Create your views here.
from django.views.generic import TemplateView

from Auth.models import User, UserAchievements
from Pay.models import PaymentHistory, BoughtItem


class ProfileView(TemplateView):
    template_name = "registration/profile.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        user = self.request.user
        context['achievements'] = UserAchievements.objects.filter(user=user)
        context['boughtItems'] = BoughtItem.objects.filter(payment_history__user=user)
        context['place'] = User.objects.filter(balance__gt=user.balance).count()

        return context