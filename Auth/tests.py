from django.test import TestCase

from Auth.models import User


class AuthTestCase(TestCase):
    def setUp(self):
        UID = '0123'
        username = 'zxnm,c'
        print(username)
        User.objects.create(username=username,UID=UID,type=0,password='qweasdzxc')
        user = User.objects.get(username=username)
        self.assertNotEquals(user.UID,UID)