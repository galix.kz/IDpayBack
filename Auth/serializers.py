from rest_framework.serializers import ModelSerializer

from Auth.models import User


class UserSerializer(ModelSerializer):
    class Meta:
        model = User
        fields = ("username","first_name","last_name","email_address","uid","type","balance")