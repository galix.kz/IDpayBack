from django.contrib.auth.base_user import BaseUserManager
from django.contrib.auth.models import AbstractUser
from django.core.validators import MaxValueValidator, MinValueValidator
from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.utils.safestring import mark_safe


class UserManager(BaseUserManager):
    def create_user(self, username, password=None, **extra_fields):
        user = self.model(
            username=username,
            **extra_fields
        )
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, username, password, **extra_fields):
        user = self.create_user(
            username=username,
            password=password,
            **extra_fields
        )
        print(username)
        user.is_superuser = True
        user.is_staff = True
        user.save(using=self._db)
        return user

    def change_balance(self, price):
        print(self)

    def get_full_name(self):
        return self.first_name + ' ' + self.last_name


class User(AbstractUser):
    class UsersType:
        ADMIN = 0
        BRANCH = 1
        STUDENT = 2

    UID = models.CharField(max_length=300, unique=False, null=True, verbose_name='Номер Карты')
    type = models.IntegerField(default=UsersType.STUDENT, verbose_name='Тип',
                               help_text='Админ-0, Филиал-1, Студент-2. По умолчанию 2')
    balance = models.IntegerField(default=0, blank=True, verbose_name='Баланс')
    objects = UserManager()
    image = models.ImageField(blank=True, null=True)

    def change_balance(self, price):
        check_balance = self.balance - price
        if check_balance < 0:
            return False
        self.balance -= price
        self.save()
        return True

    def image_tag(self):
        return mark_safe('<img src="/media/%s" width="150" height="150" />' % self.image)

    def school_group_list(self):
        tag = ''
        for group in self.school_group.all():
            print(group.id)
            tag += mark_safe('<a href="/admin/Auth/schoolgroup/' + str(group.id) + '">' + str(group.name) + '</a><br>')
        return mark_safe(tag)

    image_tag.short_description = 'Image'
    school_group_list.description = 'Школьные группы'


class UserNotInDatabase(models.Model):
    uid = models.CharField(max_length=255)
    user = models.ForeignKey('Auth.User', on_delete=models.CASCADE)
    created_date = models.DateTimeField(auto_now_add=True, null=True, blank=True)

    class Meta:
        verbose_name = "Пользователи не в базе данных"
        verbose_name_plural = "Пользователи не в базе данных"


class SchoolGroup(models.Model):
    name = models.CharField(max_length=250)
    students = models.ManyToManyField('Auth.User', related_name="school_group")

    class Meta:
        verbose_name = "Школьные группы"
        verbose_name_plural = "Школьные группы"

    def __str__(self):
        return self.name


class UserAchievements(models.Model):
    user = models.ForeignKey('Auth.User', related_name='user', verbose_name='Ученик', on_delete=models.CASCADE)
    admin = models.ForeignKey('Auth.User', related_name='admin', verbose_name='Поставил оценку',
                              on_delete=models.CASCADE)
    point = models.IntegerField(verbose_name='Балл', validators=[
            MaxValueValidator(120),
            MinValueValidator(1)
        ])
    created_date = models.DateField(null=True,  verbose_name='Дата создания')
    description = models.TextField(verbose_name='Описание')

    class Meta:
        verbose_name = "Баллы ученика"
        verbose_name_plural = "Баллы ученика"
