from import_export import resources
from .models import User


class PersonResource(resources.ModelResource):
    class Meta:
        model = User
