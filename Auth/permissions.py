from rest_framework.permissions import BasePermission

from Auth.models import User


class IsAllowedToViewCategory(BasePermission):

    def has_permission(self, request, view):
        return request.user.type == User.UsersType.CAFE \
               or request.user.type == User.UsersType.CANTEEN