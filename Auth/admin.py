from django.contrib import admin
from django.contrib.admin import ModelAdmin
from django.contrib.admin.widgets import AdminDateWidget
from django.contrib.postgres.forms.ranges import DateRangeField, RangeWidget
from django.template.response import TemplateResponse
from django.urls import path
from import_export.admin import ImportExportModelAdmin, ImportMixin
from rest_framework.authtoken.models import Token
from django.utils.decorators import method_decorator

from Auth.resources import PersonResource
from .models import User, UserAchievements, UserNotInDatabase, SchoolGroup
# Register your models here.
from django import forms
from django.contrib import admin
from django.contrib.auth.models import Group
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin
from django.contrib.auth.forms import ReadOnlyPasswordHashField
from import_export.signals import post_import
from import_export.forms import ConfirmImportForm
from django.utils.translation import gettext, gettext_lazy as _
from django.utils.encoding import force_text
from django.views.decorators.http import require_POST
from django.core.exceptions import PermissionDenied


class UserCreationForm(forms.ModelForm):
    """A form for creating new users. Includes all the required
    fields, plus a repeated password."""
    password1 = forms.CharField(label='Password', widget=forms.PasswordInput)
    password2 = forms.CharField(label='Password confirmation', widget=forms.PasswordInput)

    class Meta:
        model = User
        fields = ('email', 'type')

    def clean_password2(self):
        # Check that the two password entries match
        password1 = self.cleaned_data.get("password1")
        password2 = self.cleaned_data.get("password2")
        if password1 and password2 and password1 != password2:
            raise forms.ValidationError("Passwords don't match")
        return password2

    def save(self, commit=True):
        # Save the provided password in hashed format
        user = super().save(commit=False)
        user.set_password(self.cleaned_data["password1"])
        if commit:
            user.save()
        return user


class UserChangeForm(forms.ModelForm):
    """A form for updating users. Includes all the fields on
    the user, but replaces the password field with admin's
    password hash display field.
    """
    password = ReadOnlyPasswordHashField()

    class Meta:
        model = User
        fields = ('email', 'password', 'type', 'is_active')

    def clean_password(self):
        # Regardless of what the user provides, return the initial value.
        # This is done here, rather than on the field, because the
        # field does not have access to the initial value
        return self.initial["password"]

class AchievementsInline(admin.TabularInline):
    model = UserAchievements
    fk_name = 'user'
class UserAdmin(ImportMixin, BaseUserAdmin):
    # The forms to add and change user instances
    form = UserChangeForm
    add_form = UserCreationForm
    resource_class = PersonResource
    inlines = [AchievementsInline]
    model = User
    list_display = ('username', 'type', 'first_name', 'last_name', 'image_tag')
    list_filter = ('is_staff', 'is_superuser', 'is_active', 'groups', 'type')
    # search_fields = ('username', 'type', 'first_name', 'last_name', )
    fieldsets = [
        (None, {'fields': ('username', 'password',)}),
        (_('Personal info'), {'fields': (
            'first_name', 'last_name', 'email', 'UID', 'type', 'balance', 'image', 'image_tag', 'school_group_list')}),
        (_('Important dates'), {'fields': ('last_login', 'date_joined')}),
    ]
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('username', 'UID', 'type', 'password1', 'password2',
                       'first_name', 'last_name', 'image',)}
         ),
    )
    readonly_fields = ('image_tag', 'school_group_list')

    def get_confirm_import_form(self):
        """
        Get the form type (class) used to confirm the import.
        """
        return ConfirmImportForm

    @method_decorator(require_POST)
    def process_import(self, request, *args, **kwargs):
        """
        Perform the actual import action (after the user has confirmed the import)
        """
        print(super)
        if not self.has_import_permission(request):
            raise PermissionDenied
        import tablib
        form_type = self.get_confirm_import_form()
        confirm_form = form_type(request.POST)
        if confirm_form.is_valid():
            import_formats = self.get_import_formats()
            input_format = import_formats[
                int(confirm_form.cleaned_data['input_format'])
            ]()
            tmp_storage = self.get_tmp_storage_class()(name=confirm_form.cleaned_data['import_file_name'])
            data = tmp_storage.read(input_format.get_read_mode())
            if not input_format.is_binary() and self.from_encoding:
                data = force_text(data, self.from_encoding)
            dataset = input_format.create_dataset(data)
            data2 = tablib.Dataset()
            for x in range(0, dataset.height):
                d = list(dataset[x])
                u = User.objects.filter(username=d[2]).first()
                if u:
                    u.set_password(d[9])
                    print(d[9])
                    d[9] = u.password
                data2.append(tuple(d))
            del dataset[0:]
            dataset = dataset.stack(data2)
            result = self.process_dataset(dataset, confirm_form, request, *args, **kwargs)

            tmp_storage.remove()

            return self.process_result(result, request)

    def get_import_data_kwargs(self, request, *args, **kwargs):
        """
        Prepare kwargs for import_data.
        """
        form = kwargs.get('form')
        if form:
            kwargs.pop('form')
            return kwargs
        return {}

    def process_dataset(self, dataset, confirm_form, request, *args, **kwargs):

        res_kwargs = self.get_import_resource_kwargs(request, *args, **kwargs)
        resource = self.get_import_resource_class()(**res_kwargs)
        print(confirm_form.cleaned_data['original_file_name'])
        imp_kwargs = self.get_import_data_kwargs(request, *args, **kwargs)
        return resource.import_data(dataset,
                                    dry_run=False,
                                    raise_errors=True,
                                    file_name=confirm_form.cleaned_data['original_file_name'],
                                    user=request.user,
                                    **imp_kwargs)

    def get_form(self, request, obj=None, **kwargs):
        print(self.fieldsets)
        self.exclude = []
        if request.user.is_superuser:
            self.fieldsets.append((_('Permissions'), {'fields': ('is_active', 'is_staff', 'is_superuser',
                                                                 'groups', 'user_permissions')}), )
        return super(UserAdmin, self).get_form(request, obj, **kwargs)

    def get_search_results(self, request, queryset, search_term):
        queryset, use_distinct = super().get_search_results(request, queryset, search_term)
        print(queryset)
        print(self.model.objects.filter(first_name__icontains=search_term))
        queryset |= self.model.objects.filter(first_name__icontains=search_term)
        return queryset, use_distinct


# Now register the new UserAdmin...
admin.site.register(User, UserAdmin)


class UserAchievementsCreateForm(forms.ModelForm):
    """A form for creating new users. Includes all the required
    fields, plus a repeated password."""
    created_date = forms.DateField(
        input_formats=['%m/%d/%Y'],
        widget=forms.DateInput(attrs={'class': 'datepicker'})
    )

    class Meta:
        model = UserAchievements
        fields = ('user', 'point', 'description', 'admin' )


class UserAchievementsAdmin(admin.ModelAdmin):
    list_display = ('point', 'get_user_first_name', 'get_user_last_name', 'created_date')

    def get_user_first_name(self, obj):
        return obj.user.first_name

    def get_user_last_name(self, obj):
        return obj.user.last_name

    form = UserAchievementsCreateForm
    readonly_fields = ('admin',)
    raw_id_fields = ("user", 'admin')
    get_user_first_name.short_description = 'Имя'
    get_user_last_name.short_description = 'Фамилия'

    def save_model(self, request, obj, form, change):
        print(request.user)
        obj.admin = request.user
        print(obj.save())
        print(change)

    def render_change_form(self, request, context, *args, **kwargs):
        self.change_form_template = 'change_form.html'
        return super(UserAchievementsAdmin, self).render_change_form(request,
                                                                     context, *args, **kwargs)


class SchoolGroupAdmin(ImportMixin, admin.ModelAdmin):
    filter_horizontal = ('students',)


admin.site.register(UserAchievements, UserAchievementsAdmin)
admin.site.register(SchoolGroup, SchoolGroupAdmin)
admin.site.unregister(Group)
# admin.site.unregister(Token)
admin.site.register(UserNotInDatabase)
admin.site.site_header = 'Tesla Pay Admin'
