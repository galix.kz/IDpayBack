import random

from django.db.models.signals import post_save
from django.dispatch import receiver

from Auth.models import User, UserAchievements

@receiver(post_save, sender=UserAchievements)
def change_payment_history(sender, instance, created, **kwargs):
    if created:
        user = User.objects.get(pk=instance.user.id)
        user.balance = user.balance + instance.point
        user.save()
